package si.uni_lj.fri.pbd.miniapp2;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Calendar;
import java.util.Date;

public class AccelerationService extends Service implements SensorEventListener {

    private static final String TAG = AccelerationService.class.getSimpleName();
    private final String ACTION_MOVE="moved";
    private final String COMMAND_HORIZONTAL="horizontal";
    private final String COMMAND_VERTICAL="vertical";
    private SensorManager sm=null;
    private long time=0;
    private float[] vals=new float[3];

    //binder to allow binding from activity
    public class AccelerationServiceBinder extends Binder {
        AccelerationService getService(){
            return AccelerationService.this;
        }
    }

    private final IBinder serviceBinder=new AccelerationServiceBinder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //register listener
        sm=(SensorManager)getSystemService(SENSOR_SERVICE);
        assert sm != null;
        sm.registerListener(this,sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_NORMAL);
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //unregister listener
        if(sm!=null){
            sm.unregisterListener(this);
        }
        Log.d(TAG, "Destroying accservice");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return serviceBinder;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.sensor.getType() != Sensor.TYPE_ACCELEROMETER){
            return;
        }
        if(time==0){
            time=sensorEvent.timestamp;
            vals[0]=sensorEvent.values[0];
            vals[1]=sensorEvent.values[1];
            vals[2]=sensorEvent.values[2];
        } else{
            //check if enough time passed
            long diff=sensorEvent.timestamp-time;
            time=sensorEvent.timestamp;
            if(diff>=500){
                //calculate differentials
                int thresh=5;
                float dX=Math.abs(vals[0]-sensorEvent.values[0]);
                float dY=Math.abs(vals[1]-sensorEvent.values[1]);
                float dZ=Math.abs(vals[2]-sensorEvent.values[2]);
                vals[0]=sensorEvent.values[0];
                vals[1]=sensorEvent.values[1];
                vals[2]=sensorEvent.values[2];
                dX=dX<=thresh?0:dX;
                dY=dY<=thresh?0:dY;
                dZ=dZ<=thresh?0:dZ;
                String comm="";
                if(dX>dZ){
                    comm=COMMAND_HORIZONTAL;
                }
                if(dZ>dX){
                    comm=COMMAND_VERTICAL;
                }
                //message mediaplayerservice about the event
                if(!comm.equals("")){
                    Intent i=new Intent();
                    i.setAction(ACTION_MOVE);
                    i.putExtra("command",comm);
                    sendBroadcast(i);
                }
            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
