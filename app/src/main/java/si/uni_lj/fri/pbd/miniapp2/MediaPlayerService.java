package si.uni_lj.fri.pbd.miniapp2;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MediaPlayerService extends Service {

    private static final String TAG = MediaPlayerService.class.getSimpleName();

    private static final String channelID="mediaplayer";
    private NotificationCompat.Builder builder;
    private NotificationManager nm;
    private boolean mBound=false;
    private AccelerationService accelerationService=null;
    private final String ACTION_PLAY="play";
    private final String ACTION_STOP="stop";
    private final String ACTION_EXIT="exit";
    private MediaPlayer mediaPlayer=null;
    private static final int MSG_SONG=43;
    private Handler handler= new NotifHandler(this);
    static final int NOTIFICATION_ID=1337;
    private boolean playing;
    private boolean paused;
    private boolean gestures;
    private int songIndex;

    //broadcast receiver for catching messages from accservice
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent){
            String comm=intent.getStringExtra("command");
            assert comm != null;
            String COMMAND_HORIZONTAL = "horizontal";
            String COMMAND_VERTICAL = "vertical";
            if(comm.equals(COMMAND_HORIZONTAL)){
                pausePlaying();
                Log.d(TAG,"command horizontal");
            } else if(comm.equals(COMMAND_VERTICAL)){
                try {
                    startPlaying();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.d(TAG,"command vertical");
            }
        }
    };

    //binder to allow binding from activity
    public class MediaServiceBinder extends Binder {
        MediaPlayerService getService(){
            return MediaPlayerService.this;
        }
    }

    private final IBinder serviceBinder=new MediaServiceBinder();

    //service connection for connecting with accelerationservice
    private ServiceConnection mConnection=new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "Getting accservice binder");
            //setting service
            AccelerationService.AccelerationServiceBinder accelerationServiceBinder=(AccelerationService.AccelerationServiceBinder)iBinder;
            accelerationService=accelerationServiceBinder.getService();
            mBound=true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "AccService disconnect");
            mBound=false;
        }
    };

    @Override
    public void onCreate(){
        Log.d(TAG, "Creating mediaplayerservice");
        //default values
        paused=false;
        playing=false;
        gestures=false;
        songIndex=-1;
        //initiate notification stuff
        nm=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel();
        builder=new NotificationCompat.Builder(this, channelID);
        handler.sendEmptyMessage(MSG_SONG);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Returning mediaplayerservice binder");
        return serviceBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting mediaplayerservice");
        //check what's happened
        String action=intent.getAction();
        //nothing, just started service
        if(action==null){
            startForeground(NOTIFICATION_ID,createNotification());
        } else{
            switch (action) {
                //play or pause
                case ACTION_PLAY:
                    if(isPlaying()){
                        pausePlaying();
                    } else{
                        try {
                            startPlaying();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                //stop
                case ACTION_STOP:
                    stopPlaying();
                    break;
                //exit
                case ACTION_EXIT:
                    exit();
                    break;
                default:
                    break;
            }
            //update notification
            builder=new NotificationCompat.Builder(this,channelID);
            nm.notify(NOTIFICATION_ID,createNotification());
        }
        return Service.START_STICKY;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        //stop ticking messages every second in handler, unbind accservice, unregister receiver
        unbind();
        handler.removeMessages(MSG_SONG);
        unregisterReceiver(mMessageReceiver);
        Log.d(TAG, "Destroying mediaplayerservice");
    }

    public void startPlaying() throws IOException {
        //nothing's playing so choose a song
        if(mediaPlayer==null&&(!isPlaying()||isPaused())) {
            Random rand = new Random();
            songIndex = rand.nextInt(3);
            AssetFileDescriptor afd = getAssets().openFd(getResources().getStringArray(R.array.songnames)[songIndex]);
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.prepare();
        }
        //start playing current song from where it paused, update notification button
        if(!isPlaying()||isPaused()){
            mediaPlayer.start();
            playing=true;
            paused=false;
            builder=new NotificationCompat.Builder(this,channelID);
            nm.notify(NOTIFICATION_ID,createNotification());
        }
    }

    //pause current song, update notification button
    public void pausePlaying(){
        if(mediaPlayer!=null&&isPlaying()&&!isPaused()){
            mediaPlayer.pause();
            playing=false;
            paused=true;
            builder=new NotificationCompat.Builder(this,channelID);
            nm.notify(NOTIFICATION_ID,createNotification());
        }
    }

    //stop current playing song, update notification to default
    public void stopPlaying(){
        if(mediaPlayer!=null&&(isPlaying()||isPaused())){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            playing = false;
            paused = false;
            nm.notify(NOTIFICATION_ID,createNotification());
        }
    }

    public void exit(){
        //stop current playing song
        if(mediaPlayer!=null&&(isPlaying()||isPaused())){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            playing = false;
            paused = false;
        }
        //kill everything we possibly can, get activity to kill everything as well
        handler.removeMessages(MSG_SONG);
        nm.cancel(NOTIFICATION_ID);
        Intent intent = new Intent();
        intent.setAction(ACTION_EXIT);
        sendBroadcast(intent);
    }

    //toggles gestures on, starts and binds accservice, registers receiver for getting messages from accservice
    public void toggleGestureOn(){
        if(!mBound&&!gesturesOn()) {
            bind();
            IntentFilter inf=new IntentFilter();
            String ACTION_MOVE = "moved";
            inf.addAction(ACTION_MOVE);
            registerReceiver(mMessageReceiver, inf);
            mBound=true;
            gestures=true;
        }
    }

    //toggles gestures off, stops and unbinds accservice, unregisters receiver
    public void toggleGestureOff(){
        if(mBound&&gesturesOn()){
            unbind();
            unregisterReceiver(mMessageReceiver);
            mBound=false;
            gestures=false;
        }
    }

    //binds this service with accelerationservice instance
    private void bind(){
        if(!mBound){
            Log.d(TAG, "Starting and binding accservice");
            Intent i = new Intent(getApplicationContext(), AccelerationService.class);
            startService(i);
            bindService(i, mConnection, 0);
            mBound=true;
        }
    }

    //unbinds accservice from this service and stops it
    private void unbind(){
        if(mBound){
            Log.d(TAG, "Unbinding accservice");
            unbindService(mConnection);
            accelerationService=null;
            Intent i=new Intent(getApplicationContext(), AccelerationService.class);
            stopService(i);
            mBound=false;
        }
    }

    //returns the index and name of song file playing or paused now
    public String currentSong(){
        return (isPlaying()||isPaused())?((songIndex+1)+". "+getResources().getStringArray(R.array.songnames)[songIndex]):getString(R.string.song);
    }

    //returns the duration and time position of song file playing or paused now
    public String currentDuration(){
        if(mediaPlayer==null){
            return getString(R.string.time);
        } else{
            int full=mediaPlayer.getDuration();
            int now=mediaPlayer.getCurrentPosition();
            return formatted(now)+"/"+formatted(full);
        }
    }

    public boolean gesturesOn(){
        return gestures;
    }

    public boolean isPlaying(){
        return playing;
    }

    public boolean isPaused(){
        return paused;
    }

    //format miliseconds to hour minute second
    private String formatted(int ms){
        long secs=TimeUnit.MILLISECONDS.toSeconds(ms);
        long hours=secs/3600;
        secs=secs%3600;
        long minutes=secs/60;
        secs=secs%60;
        return String.format(Locale.getDefault(),"%d:%02d:%02d",hours,minutes,secs);
    }

    //update song and duration in notification and send broadcast to update activity UI
    private void updateSongText(){
        builder.setContentText(currentSong())
                .setSubText(currentDuration());
        nm.notify(NOTIFICATION_ID,createNotification());
        Intent intent = new Intent();
        String ACTION_UPDATE = "update";
        intent.setAction(ACTION_UPDATE);
        intent.putExtra("song",currentSong());
        intent.putExtra("duration",currentDuration());
        sendBroadcast(intent);
    }

    //make a new notification that reflects current status with song duration button names etc
    private Notification createNotification() {
        Intent playIntent=new Intent(this,MediaPlayerService.class);
        playIntent.setAction(ACTION_PLAY);
        PendingIntent playPendingIntent=PendingIntent.getService(this,0,playIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(isPlaying()?android.R.drawable.ic_media_pause:android.R.drawable.ic_media_play,isPlaying()?getString(R.string.b_pause):getString(R.string.b_play),playPendingIntent);

        Intent stopIntent=new Intent(this,MediaPlayerService.class);
        stopIntent.setAction(ACTION_STOP);
        PendingIntent stopPendingIntent=PendingIntent.getService(this,0,stopIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(R.drawable.ic_stat_name,getString(R.string.b_stop),stopPendingIntent);

        Intent exitIntent=new Intent(this,MediaPlayerService.class);
        exitIntent.setAction(ACTION_EXIT);
        PendingIntent exitPendingIntent=PendingIntent.getService(this,0,exitIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(android.R.drawable.button_onoff_indicator_off,getString(R.string.b_exit),exitPendingIntent);

        builder.setSmallIcon(R.drawable.unilj_logo)
                .setContentTitle(getString(R.string.notif_title))
                .setContentText(currentSong())
                .setSubText(currentDuration())
                .setOnlyAlertOnce(true)
                .setChannelId(channelID);

        Intent actIntent = new Intent(this, MainActivity.class);
        PendingIntent actPendingIntent = PendingIntent.getActivity(this, 0, actIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(actPendingIntent);

        return builder.build();
    }

    //makes notification channel
    private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT < 26) {
            return;
        } else {

            NotificationChannel channel = new NotificationChannel(MediaPlayerService.channelID, getString(R.string.channel), NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(getString(R.string.channeld));
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);

            assert nm != null;
            nm.createNotificationChannel(channel);
        }
    }

    //handler that ticks every second and calls function to update notification and activity UI
    static class NotifHandler extends Handler {

        private final static int UPDATE_RATE_MS = 1000;
        private final WeakReference<MediaPlayerService> service;

        NotifHandler(MediaPlayerService service) {
            this.service = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message message) {
            if (MSG_SONG == message.what) {
                service.get().updateSongText();
                sendEmptyMessageDelayed(MSG_SONG, UPDATE_RATE_MS);
            }
        }
    }

}
