package si.uni_lj.fri.pbd.miniapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private final String ACTION_UPDATE="update";
    private final String ACTION_EXIT="exit";
    private boolean mBound=false;
    private MediaPlayerService mediaPlayerService=null;
    private TextView song;
    private TextView duration;

    //broadcast receiver for mediaplayerservice broadcasts
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //check which action it is
            String action=intent.getAction();
            assert action != null;
            //update UI
            if(action.equals(ACTION_UPDATE)){
                String song=intent.getStringExtra("song");
                String duration=intent.getStringExtra("duration");
                updateSong(song,duration);
            }
            //kill everything
            else if(action.equals(ACTION_EXIT)){
                if(!mBound){
                    bind();
                }
                if(mediaPlayerService!=null){
                    mediaPlayerService.stopPlaying();
                    Intent i = new Intent(getApplicationContext(), MediaPlayerService.class);
                    stopService(i);
                }
                finishAffinity();
            }
        }
    };

    //service connection for doing stuff when binding mediaplayerservice
    private ServiceConnection mConnection=new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "Getting mediaservice binder");
            //setting service
            MediaPlayerService.MediaServiceBinder mediaServiceBinder =(MediaPlayerService.MediaServiceBinder)iBinder;
            mediaPlayerService=mediaServiceBinder.getService();
            mBound=true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "MediaService disconnect");
            mBound=false;
        }
    };

    //binds activity with mediaplayerservice instance
    private void bind(){
        if(!mBound){
            Log.d(TAG, "Starting and binding mediaservice");
            Intent i = new Intent(getApplicationContext(), MediaPlayerService.class);
            startService(i);
            bindService(i, mConnection, 0);
        }
    }

    //unbinds activity from mediaplayerservice instance
    private void unbind(){
        Log.d(TAG, "Unbinding mediaservice");
        unbindService(mConnection);
        mBound=false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //find views that are needed after
        duration=findViewById(R.id.time);
        song=findViewById(R.id.song);
        Button playButton = findViewById(R.id.play);
        Button pauseButton = findViewById(R.id.pause);
        Button stopButton = findViewById(R.id.stop);
        Button exitButton = findViewById(R.id.exit);
        Button gestureonbutton=findViewById(R.id.gon);
        Button gestureoffbutton=findViewById(R.id.goff);

        //set onclick functions
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    playClick(view);
                } catch (IOException e) {
                    e.printStackTrace();e.printStackTrace();
                }
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pauseClick(view);
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopClick(view);
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitClick(view);
            }
        });

        gestureonbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleGestureOn(view);
            }
        });

        gestureoffbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                toggleGestureOff(view);
            }
        });

    }

    //bind on start
    @Override
    public void onStart(){
        super.onStart();
        bind();
        //register broadcast receiver for mediaplayerservice
        IntentFilter inf=new IntentFilter();
        inf.addAction(ACTION_EXIT);
        inf.addAction(ACTION_UPDATE);
        registerReceiver(mMessageReceiver, inf);
    }

    //unbind and unregister receiver
    @Override
    public void onDestroy(){
        super.onDestroy();
        unbind();
        unregisterReceiver(mMessageReceiver);
    }

    // start playing a song in mediaplayerservice
    private void playClick(View view) throws IOException {
        if(!mBound){
            bind();
        }
        if(mediaPlayerService!=null&&(!mediaPlayerService.isPlaying()||mediaPlayerService.isPaused())){
            mediaPlayerService.startPlaying();
        }
    }

    //pause playing a song in mediaplayerservice
    private void pauseClick(View view){
        if(!mBound){
            bind();
        }
        if(mediaPlayerService!=null&&!mediaPlayerService.isPaused()&&mediaPlayerService.isPlaying()){
            mediaPlayerService.pausePlaying();
        }
    }

    //stop playing a song in mediaplayerservice
    private void stopClick(View view){
        if(!mBound){
            bind();
        }
        if(mediaPlayerService!=null&&(mediaPlayerService.isPlaying()||mediaPlayerService.isPaused())){
            mediaPlayerService.stopPlaying();
            resetSong();
        }
    }

    //stop everything in mediaplayerservice, kill everything that we can
    private void exitClick(View view){
        if(!mBound){
            bind();
        }
        if(mediaPlayerService!=null){
            mediaPlayerService.stopPlaying();
            Intent i = new Intent(getApplicationContext(), MediaPlayerService.class);
            stopService(i);
        }
        finishAffinity();
    }

    //signals mediaplayerservice to toggle gestures on, shows message
    private void toggleGestureOn(View view){
        if(!mBound){
            bind();
        }
        if(mediaPlayerService!=null&&!mediaPlayerService.gesturesOn()){
            mediaPlayerService.toggleGestureOn();
            Toast.makeText(getApplicationContext(),getString(R.string.gon),Toast.LENGTH_SHORT).show();
        }
    }

    //signals mediaplayerservice to toggle gestures off, shows message
    private void toggleGestureOff(View view){
        if(!mBound){
            bind();
        }
        if(mediaPlayerService!=null&&mediaPlayerService.gesturesOn()){
            mediaPlayerService.toggleGestureOff();
            Toast.makeText(getApplicationContext(),getString(R.string.goff),Toast.LENGTH_SHORT).show();
        }
    }

    //resets UI to default
    private void resetSong(){
        song.setText(getString(R.string.song));
        duration.setText(getString(R.string.time));
    }

    //updates UI to song data
    public void updateSong(String s,String d){
        song.setText(s);
        duration.setText(d);
    }
}
